import React, {useEffect, useState} from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import Cookies from "universal-cookie";
const cookies = new Cookies();

//получаем токен, сгенерированный по логину
const token = cookies.get("TOKEN");

export default function AuthComponent() {
    //установить начальное состояние для сообщения, которое мы получим после вызова API
    const [message, setMessage] = useState("");

    //useEffect автоматически выполняется после полной загрузки страницы.
    useEffect(() => {
        //здесь устанавливаем конфигурации для вызова API
        const configuration = {
            method: "get",
            url: "http://localhost:3001/auth-endpoint",
            headers: {
                authorization: `Bearer ${token}`
            }
        }

        // делаем вызов API
        axios(configuration)
        .then((result) => {
            // присваиваем сообщение в нашем результате сообщению, которое мы инициализировали выше
            setMessage(result.data.message); 
        })
        .catch((error) => {
            error = new Error();
        });
    }, [])

    //снятие авторизации
    const logout = () => {
        //уничтожаем куки
        cookies.remove("TOKEN", {path: "/"});
        //перенаправляем пользователя на основную станицу проекта
        window.location.href = "/";
    }

    return (
        <div className="text-center">
            <h1>Auth Component</h1>

            {/* отображение нашего сообщения из нашего вызова API */}
            <h3 className="text-danger">{message}</h3>

            {/* снятие авторизации */}
            <Button type="submit" variant="danger" onClick={() => logout()}>
                Logout
            </Button>
        </div>
    )
}