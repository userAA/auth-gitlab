import React, {useEffect, useState} from "react";
import axios from "axios";

export default function FreeComponent() {
    const [message, setMessage] = useState("");

    //useEffect автоматически выполняется после полной загрузки страницы
    useEffect(() => {        
        //здесь устанавливаем конфигурации для вызова API
        const configuration = {
            method: "get",
            url: "http://localhost:3001/free-endpoint"
        };

        // делаем вызов API
        axios(configuration)
        .then((result) => {            
            //присваиваем сообщение в нашем результате сообщению, которое мы инициализировали выше
            setMessage(result.data.message);
        })
        .catch((error) => {
            error = new Error();
        })        
    }, [])

    return (
        <div>
            <h1 className = "text-center">Free Component</h1>
            {/* отображение нашего сообщения из нашего вызова API */}
            <h3 className="text-center text-danger">{message}</h3>
        </div>
    )
}