import React from "react";
import {Route, Redirect} from "react-router-dom";
import Cookies from "universal-cookie";
const cookies = new Cookies();

//получает component и любые другие реквизиты, представленные ...rest
export default function ProtectedRoutes({component: Component, ...rest})
{
    return (
        // этот маршрут принимает другие маршруты, назначенные ему из App.js, и возвращает тот же
        <Route
            {...rest}
            render = {(props) => {
                //получить куки из браузера, если пользователь авторизован
                const token = cookies.get("TOKEN");
   
                //возвращает маршрут, если в cookie установлен токен
                if (token)
                {
                    return <Component {...props}/>;
                }
                else
                {
                    //перенаправляет пользователя на целевую страницу, если нет действительного токена
                    return (
                        <Redirect
                            to={{
                                pathname: "/",
                                state: {                                    
                                    //установка местоположения, к которому пользователь собирался получить доступ перед перенаправлением
                                    from: props.location
                                }
                            }}
                        />
                    )                    
                }
            }}
        />
    )
}