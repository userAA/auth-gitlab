import express from "express";
import bcrypt from "bcrypt";
import bodyParser from "body-parser";
import jwt from "jsonwebtoken";
import cors from "cors";
import auth from "./auth.js";

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// требуется подключение к базе данных
import dbConnect from "./db/dbConnect.js";
//выполняем подключение к базе данных
dbConnect();

import Users from "./db/userModel.js";

//регистрация пользователя
app.post("/register", (request, response) => {            
    //ищем пользователя в модели базы данных UsersModel
    const userExist =  Users.findOne({ user: request.body.email});
    //существующего пользователя удаляем из базы данных
    if (userExist != undefined)
    {
        Users.findOneAndDelete(
        {
            email: request.body.email
        },
        (err, doc) => {
        });
    }

    //шифруем пароль
    bcrypt.hash(request.body.password,10).then((hashedPassword) => {
        //создаем новый экземпляр пользователя и собираем данные
        const user = new Users({
            email: request.body.email,
            password: hashedPassword
        });

        //записываем нового пользователя
        user.save()
        // возвращаем успех, если новый пользователь успешно добавлен в базу данных
        .then((result) => {
            response.status(201).send({
                message: "User Created Successfully",
                result
            })
        })
        //отловим ошибку, если новый пользователь не был успешно добавлен в базу данных
        .catch((error) => {
            response.status(500).send({
                message: "Error creating user",
                error
            })
        })
    })
    //отловим ошибку, если пароль не защифровался
    .catch((e) => {
        response.status(500).send({
            message: "Password was not hashed successfully",
            e
        })
    })
})

//авторизация пользователя
app.post("/login", (request, response) => {
    //проверяем, существует ли почта
    Users.findOne({email: request.body.email})
    //если почта существует
    .then((user) => {        
        //сравним введенный пароль и найденный зашиврованный пароль
        bcrypt.compare(request.body.password, user.password)
        //если пароли совпадают
        .then((passwordCheck) => {
            //проверяем совпадают ли пароли
            if (!passwordCheck) {
                return response.status(400).send({
                    message: "Passwords does not match",
                    error
                })
            }

            //создаем JWT токен
            const token = jwt.sign(
                {
                    userId: user._id,
                    userEmail: user.email
                },
                "RANDOM-TOKEN",
                {expiresIn: "24h"}
            );

            // возвращаем ответ об успешном завершении
            response.status(200).send({
                message: "Login Successful",
                email: user.email,
                token
            })
        })
        // ловим ошибку, если пароль не совпадает
        .catch((error) => {
            response.status(400).send({
                message: "Passwords does not match",
                error
            })
        })
    })
    //обнаруживаем ошибку, если электронная почта не существует
    .catch((e) => {
        response.status(404).send({
            message: "Email not found",
            e
        })
    })
})

//свободный запрос
app.get("/free-endpoint", (_, response) => {
    response.json({message: "You are free to access me anytime"});
});

//запрос на проверку авторизации
app.get("/auth-endpoint", auth, (_, response) => {
    response.json({message: "You are authorized to access me"});
})

//запускаем сервер по порту 3001
const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))