import jwt from "jsonwebtoken";

const auth = async (request, response, next) => {
    try
    {
        //получить токен из заголовка авторизации
        const token = await (request.headers.authorization || '').replace(/Bearer\s?/,'');
    
        //проверяем, соответствует ли токен предполагаемому исходному
        const decodedToken = await jwt.verify(token , "RANDOM-TOKEN");

        //извлекаем данные об авторизованном пользователе
        const user = await decodedToken;

        // передаем пользователя вниз к концу функции
        request.user = user;

        //передать функциональность к концу функции
        next();
    }
    catch (error)
    {
        response.status(401).json({
            error: new Error("Invalid request!")
        })
    }
}

export default auth;